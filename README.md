# Pasos para instalar ArgoCD y conectar con el RHSSO de ocp


## 1. Recupero si quiero la clave de admin de argocd
```sh
oc get secret  openshift-gitops-cluster -o yaml -n openshift-gitops | grep admin.password | awk '{print $2}'  | base64 -d ; echo ""
```

## 2. Creo grupo cluster-admins (sin asignar ningun rol binding)
```sh
kind: Group
apiVersion: user.openshift.io/v1
metadata:
  name: cluster-admins
users:
  - admin
```
## 3. Asigno a la SA openshift-gitops-argocd-application-controller
cluster rol:
 * self-provisioners
 * admin

## 4. Borro los pods
```sh
oc delete pods --all
```

## 5. Me logueo con admin / clave del paso ## 1.

## 6. Creo una Application para probar permisos
```sh
1. NewApp
Project Name: default (se refiere al proyecto de argocd)
Sync: Automatic
Prune: on
Self Heal: on
Auto-Create Namespace: on

repo: https://github.com/helm/examples
revision: main o HEAD
path: charts/hello-world

cluster: https://kubernetes.default.svc
namespace: mosco
```

## 7. Creo un ApplicationSet
oc apply -f backup/manager.yaml

## 8. Nota
A partir de ahora argocd va a monitorear el repo en la carpeta applicationset, si creamos una nueva ApplicationSet, este la crea en ocp.  
Aunque se cree dentro de una carpeta ArgoCD lo encuentra igual

## 9. Ver
https://medium.com/@aaltundemir/demystifying-gitops-bootstrapping-argo-cd-4a861284f273  